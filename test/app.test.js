/* eslint-env mocha */
const supertest = require('supertest');
const expect = require('chai').expect;
const app = require('../server');


const request = supertest.agent(app.listen());

describe('API', () => {
  describe('/', () => {
    it('should say "OK"', async () => {
      const response = await request.get('/').expect(200);
      expect(response.text).to.be.a('string');
    });
  });
});